lazy val root = (project in file("."))
  .enablePlugins(PlayScala, JavaAppPackaging)
  .settings(
    name := "recipes",
    version := "1.0-SNAPSHOT",
    scalaVersion := "2.13.8",
    libraryDependencies ++= Seq(
      guice,
      jdbc,
      evolutions,
      "mysql" % "mysql-connector-java" % "8.0.29",
      "org.playframework.anorm" %% "anorm" % "2.6.10",
      "org.scalatestplus.play" %% "scalatestplus-play" % "5.1.0" % Test,
      "org.scalamock" %% "scalamock" % "5.2.0" % Test,
      "org.scalatestplus" %% "mockito-4-5" % "3.2.12.0" % Test
    ),
    scalacOptions ++= List("-encoding", "utf8", "-deprecation", "-feature", "-unchecked", "-Xfatal-warnings"),
    javacOptions ++= List("-Xlint:unchecked", "-Xlint:deprecation", "-Werror")
  )

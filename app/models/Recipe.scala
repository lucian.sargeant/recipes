package models

import anorm.{Macro, ToParameterList}
import play.api.libs.json.{Json, OFormat}

import java.time.LocalDateTime

case class Recipe(var id: Int,
                  title: String,
                  making_time: String,
                  serves: String,
                  ingredients: String,
                  cost: Int,
                  created_at: LocalDateTime = LocalDateTime.now(),
                  updated_at: LocalDateTime = LocalDateTime.now())

object Recipe {

  implicit def toParameters: ToParameterList[Recipe] = Macro.toParameters[Recipe]

  implicit val responseFormat: OFormat[Recipe] = Json.format[Recipe]
}

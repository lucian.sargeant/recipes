package models

import play.api.libs.json.{Json, OFormat}

case class RecipeResponse(message: String, recipe: List[Recipe])

object RecipeResponse {

  val RecipeDetails = "Recipe details by id"
  val RecipeCreated = "Recipe successfully created!"
  val RecipeUpdated = "Recipe successfully updated!"

  implicit val responseFormat: OFormat[RecipeResponse] = Json.format[RecipeResponse]

  def details(recipe: Recipe): RecipeResponse = RecipeResponse(RecipeDetails, List(recipe))

  def created(recipe: Recipe): RecipeResponse = RecipeResponse(RecipeCreated, List(recipe))

  def updated(recipe: Recipe): RecipeResponse = RecipeResponse(RecipeUpdated, List(recipe))
}



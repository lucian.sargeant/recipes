package models

import play.api.libs.json.{Json, OFormat}

case class RecipeActionResponse(message: String, required: Option[String] = None)

object RecipeActionResponse {

  val RecipeCreationFailed = "Recipe creation failed!"
  val RecipeUpdateFailed = "Recipe update failed!"

  implicit val responseFormat: OFormat[RecipeActionResponse] = Json.format[RecipeActionResponse]

  def of(message: String, missingFields: Option[Set[String]] = None): RecipeActionResponse = {
    RecipeActionResponse(message, missingFields.map(fields => fields.mkString(", ")))
  }
}

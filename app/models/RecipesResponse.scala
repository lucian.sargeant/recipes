package models

import play.api.libs.json.{Json, OFormat}

case class RecipesResponse(recipes: List[Recipe])

object RecipesResponse {

  implicit val responseFormat: OFormat[RecipesResponse] = Json.format[RecipesResponse]
}

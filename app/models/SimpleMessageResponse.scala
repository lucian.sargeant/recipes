package models

import play.api.libs.json.{Json, OFormat}

case class SimpleMessageResponse(message: String)

object SimpleMessageResponse {

  val Deleted = "Recipe successfully removed!"
  val NotFound = "No recipe found"

  implicit val responseFormat: OFormat[SimpleMessageResponse] = Json.format[SimpleMessageResponse]

  def deleted: SimpleMessageResponse = SimpleMessageResponse(Deleted)
  def notFound: SimpleMessageResponse = SimpleMessageResponse(NotFound)
}

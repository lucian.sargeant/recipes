package models

import play.api.libs.json._

case class RecipeRequest(title: String, making_time: String, serves: String, ingredients: String, cost: Int) {

  def toRecipe: Recipe = Recipe(0, title, making_time, serves, ingredients, cost)

  def toRecipe(recipeId: Int): Recipe = Recipe(recipeId, title, making_time, serves, ingredients, cost)
}

object RecipeRequest {

  implicit val requestFormat: OFormat[RecipeRequest] = Json.format[RecipeRequest]
}

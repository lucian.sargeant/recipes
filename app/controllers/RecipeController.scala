package controllers

import models.RecipeActionResponse.{RecipeCreationFailed, RecipeUpdateFailed}
import models._
import play.api.libs.json._
import play.api.mvc._
import repositories.RecipeRepository
import services.RecipeDataService

import javax.inject._
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class RecipeController @Inject()(val recipeRepository: RecipeRepository,
                                 val recipeDataService: RecipeDataService,
                                 val controllerComponents: ControllerComponents)(implicit ec: ExecutionContext)
  extends BaseController {

  private val logger = play.api.Logger(this.getClass)

  def getAll: Action[AnyContent] = Action.async { implicit request =>

    logger.logger.info("Fetching all recipes.")

    recipeRepository.getAll.map(recipes => Ok(Json.toJson(RecipesResponse(recipes))))
  }

  def getById(recipeId: Int): Action[AnyContent] = Action.async { implicit request =>

    logger.logger.info("Fetching recipe with ID: {}.", recipeId)

    recipeRepository.findById(recipeId).map(result =>
      result.map(recipe => Ok(Json.toJson(RecipeResponse.details(recipe)))).getOrElse(NotFound))
  }

  def create: Action[AnyContent] = Action.async { implicit request =>

    logger.logger.info("Creating new recipe.")

    val missingFields = checkForMissingFields(request)

    if (missingFields.isEmpty) {

      parseRecipeRequest(request).map(createRecipe).getOrElse(Future.successful(creationFailed()))
    } else {
      Future.successful(creationFailed(missingFields))
    }
  }

  def update(recipeId: Int): Action[AnyContent] = Action.async { implicit request =>

    logger.logger.info("Updating recipe with ID: {}.", recipeId)

    val missingFields = checkForMissingFields(request)

    if (missingFields.isEmpty) {

      parseRecipeRequest(request).map(updateRequest => {

        val recipe = updateRequest.toRecipe

        updateRecipe(recipeId, recipe)

      }).getOrElse(Future.successful(updateFailed()))
    } else {
      Future.successful(updateFailed(missingFields))
    }
  }

  def delete(recipeId: Int): Action[AnyContent] = Action.async { implicit request =>

    logger.logger.info("Deleting recipe with ID: {}.", recipeId)

    recipeRepository.delete(recipeId).map(deletionCount =>
      if (deletionCount == 1) {
        Ok(Json.toJson(SimpleMessageResponse.deleted))
      } else {
        Ok(Json.toJson(SimpleMessageResponse.notFound))
      })
  }

  private def createRecipe(request: RecipeRequest): Future[Result] = {

    recipeDataService.save(request.toRecipe).map(result => result
      .map(recipe => Ok(Json.toJson(RecipeResponse.created(recipe))))
      .getOrElse(creationFailed()))
  }

  private def updateRecipe(recipeId: Int, recipe: Recipe): Future[Result] = {

    recipeRepository.update(recipeId, recipe).map(updateCount =>
      if (updateCount == 1) {
        Ok(Json.toJson(RecipeResponse.updated(recipe)))
      } else {
        Ok(Json.toJson(SimpleMessageResponse.notFound))
      })
  }

  private def checkForMissingFields(request: Request[AnyContent]): Set[String] = {
    val RequiredFields: Set[String] = Set("title", "making_time", "serves", "ingredients", "cost")

    request.body.asJson.map(json => RequiredFields.diff(json.asInstanceOf[JsObject].keys)).getOrElse(Set())
  }

  private def parseRecipeRequest(request: Request[AnyContent]): Option[RecipeRequest] = {
    request.body.asJson.flatMap(Json.fromJson[RecipeRequest](_).asOpt)
  }

  private def creationFailed(missingFields: Set[String]) = {
    Ok(Json.toJson(RecipeActionResponse.of(RecipeCreationFailed, Option(missingFields))))
  }

  private def creationFailed() = {
    Ok(Json.toJson(RecipeActionResponse.of(RecipeCreationFailed)))
  }

  private def updateFailed(missingFields: Set[String]) = {
    Ok(Json.toJson(RecipeActionResponse.of(RecipeUpdateFailed, Option(missingFields))))
  }

  private def updateFailed() = {
    Ok(Json.toJson(RecipeActionResponse.of(RecipeUpdateFailed)))
  }
}

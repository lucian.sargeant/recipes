package repositories

import anorm.SqlParser.get
import anorm._
import models.Recipe
import play.api.db.DBApi

import java.time.LocalDateTime
import javax.inject.{Inject, Singleton}
import scala.concurrent.Future

@Singleton
class RecipeRepository @Inject()(dbapi: DBApi)(implicit ec: DatabaseExecutionContext) {

  private val db = dbapi.database("default")

  private val recipeParser = {
    get[Int]("recipes.id") ~
      get[String]("recipes.title") ~
      get[String]("recipes.making_time") ~
      get[String]("recipes.serves") ~
      get[String]("recipes.ingredients") ~
      get[Int]("recipes.cost") ~
      get[LocalDateTime]("recipes.created_at") ~
      get[LocalDateTime]("recipes.updated_at") map {
      case id ~ title ~ makingTime ~ serves ~ ingredients ~ cost ~ createdAt ~ updatedAt =>
        Recipe(id, title, makingTime, serves, ingredients, cost, createdAt, updatedAt)
    }
  }

  def getAll: Future[List[Recipe]] = Future {
    db.withConnection { implicit connection =>
      SQL"SELECT * FROM recipes".as(recipeParser.*)
    }
  }

  def findById(id: Int): Future[Option[Recipe]] = Future {
    db.withConnection { implicit connection =>
      SQL"SELECT * FROM recipes WHERE id = $id".as(recipeParser.singleOpt)
    }
  }

  def insert(recipe: Recipe): Future[Option[Long]] = Future {
    db.withConnection { implicit connection =>
      SQL("""
        INSERT INTO recipes
          (title, making_time, serves, ingredients, cost, created_at, updated_at)
          VALUES ({title}, {making_time}, {serves}, {ingredients}, {cost}, {created_at}, {updated_at})
      """).bind(recipe).executeInsert()
    }
  }

  def update(id: Int, recipe: Recipe): Future[Int] = Future {
    db.withConnection { implicit connection =>
      SQL(
        """
        UPDATE recipes SET
          title = {title},
          making_time = {making_time},
          serves = {serves},
          ingredients = {ingredients},
          cost = {cost}
        WHERE id = {id}
      """).bind(recipe.copy(id = id)).executeUpdate()
    }
  }

  def delete(id: Int): Future[Int] = Future {
    db.withConnection { implicit connection =>
      SQL"DELETE FROM recipes WHERE id = $id".executeUpdate()
    }
  }
}

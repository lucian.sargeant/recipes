package services

import models.Recipe
import repositories.RecipeRepository

import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class RecipeDataService @Inject()(val recipeRepository: RecipeRepository)(implicit ec: ExecutionContext) {

  def save(recipe: Recipe): Future[Option[Recipe]] = {
    recipeRepository.insert(recipe)
      .map(result => result
        .map(recipeId => {
          recipe.id = recipeId.toInt
          recipe
        }))
  }
}

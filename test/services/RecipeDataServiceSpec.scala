package services

import models.Recipe
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito.when
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.PlaySpec
import repositories.RecipeRepository
import utils.TestInjector
import utils.TestUtils.{MaxAsyncTimeToWait, mockRecipe}

import scala.concurrent.{Await, ExecutionContext, Future}

class RecipeDataServiceSpec extends PlaySpec with TestInjector with MockitoSugar {

  val mockRecipeRepository: RecipeRepository = mock[RecipeRepository]

  implicit val executionContext: ExecutionContext = injector.instanceOf[ExecutionContext]

  val recipeDataService: RecipeDataService = new RecipeDataService(mockRecipeRepository)

  "A call to save a new recipe" must {
    "return the recipe if the insert is successful" in {

      val recipe: Recipe = mockRecipe(1)

      when(mockRecipeRepository.insert(any())).thenReturn(Future.successful(Option(1)))

      val newRecipe = Await.result(recipeDataService.save(recipe), MaxAsyncTimeToWait)

      newRecipe shouldBe Some(recipe)
    }
    "return none if the insert fails" in {

      val recipe: Recipe = mockRecipe(1)

      when(mockRecipeRepository.insert(any())).thenReturn(Future.successful(Option.empty))

      val newRecipe = Await.result(recipeDataService.save(recipe), MaxAsyncTimeToWait)

      newRecipe mustBe None
    }
  }
}

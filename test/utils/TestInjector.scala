package utils

import play.api.Mode
import play.api.inject.Injector
import play.api.inject.guice.GuiceApplicationBuilder

trait TestInjector {

  val injector: Injector = new GuiceApplicationBuilder()
    .in(Mode.Test)
    .injector()
}

package utils

import models.{Recipe, RecipeRequest}
import play.api.libs.json.Json
import play.api.mvc.AnyContent
import play.api.test.FakeRequest

import scala.concurrent.duration.{Duration, FiniteDuration}

object TestUtils {

  implicit val MaxAsyncTimeToWait: FiniteDuration = Duration(1, "second")

  def mockRecipe(recipeId: Int): Recipe = Recipe(
    recipeId,
    "Chicken Curry",
    "45 min",
    "4 people",
    "onion, chicken, seasoning",
    1000
  )

  def mockRecipeRequest(): FakeRequest[AnyContent] = FakeRequest()
    .withHeaders("Content-type" -> "application/json")
    .withJsonBody(Json.toJson(RecipeRequest(
      "Rice Omelette",
      "30 min",
      "2 people",
      "onion, egg, seasoning, soy sauce",
      700
  )))
}

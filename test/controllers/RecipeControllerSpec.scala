package controllers

import akka.http.scaladsl.model.StatusCodes.{NotFound, OK}
import models.RecipeActionResponse.RecipeCreationFailed
import models.RecipeResponse.{RecipeCreated, RecipeDetails, RecipeUpdated}
import models.SimpleMessageResponse
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito.when
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.PlaySpec
import play.api.mvc.ControllerComponents
import play.api.test.FakeRequest
import play.api.test.Helpers.{contentAsString, status, stubControllerComponents}
import repositories.RecipeRepository
import services.RecipeDataService
import utils.TestInjector
import utils.TestUtils.{MaxAsyncTimeToWait, mockRecipe, mockRecipeRequest}

import scala.concurrent.{ExecutionContext, Future}

class RecipeControllerSpec extends PlaySpec with TestInjector with MockitoSugar with ScalaFutures {

  implicit val executionContext: ExecutionContext = injector.instanceOf[ExecutionContext]

  val mockRecipeRepository: RecipeRepository = mock[RecipeRepository]
  val mockRecipeDataService: RecipeDataService = mock[RecipeDataService]
  val mockControllerComponents: ControllerComponents = mock[ControllerComponents]

  val recipeController: RecipeController = new RecipeController(
    mockRecipeRepository,
    mockRecipeDataService,
    stubControllerComponents()
  )

  "A request to fetch all recipes" must {
    "return a list of recipes if any exist" in {
      val existingRecipes = List(mockRecipe(1), mockRecipe(2))

      when(mockRecipeRepository.getAll).thenReturn(Future.successful(existingRecipes))

      val results = recipeController.getAll(FakeRequest())

      status(results)(MaxAsyncTimeToWait) must equal(OK.intValue)
      contentAsString(results)(MaxAsyncTimeToWait) must
        (include("recipes") and include("\"id\":1") and include("\"id\":2"))
    }
    "return an empty list if none exist" in {

      when(mockRecipeRepository.getAll).thenReturn(Future.successful(List()))

      val results = recipeController.getAll(FakeRequest())

      status(results)(MaxAsyncTimeToWait) must equal(OK.intValue)
      contentAsString(results)(MaxAsyncTimeToWait) must equal("{\"recipes\":[]}")
    }
  }

  "A request to fetch a recipe by id" must {
    "return the specified recipe if it exists" in {

      val recipe = mockRecipe(1)

      when(mockRecipeRepository.findById(any())).thenReturn(Future.successful(Option(recipe)))

      val result = recipeController.getById(1)(FakeRequest())

      status(result)(MaxAsyncTimeToWait) must equal(OK.intValue)
      contentAsString(result)(MaxAsyncTimeToWait) must
        (include(RecipeDetails) and include("\"id\":1"))
    }
    "return a message indicating the recipe wasn't found" in {

      when(mockRecipeRepository.findById(any())).thenReturn(Future.successful(Option.empty))

      val result = recipeController.getById(999)(FakeRequest())

      status(result)(MaxAsyncTimeToWait) must equal(NotFound.intValue)
    }
  }

  "A request to create a recipe" must {
    "return the newly created recipe when successful" in {

      when(mockRecipeDataService.save(any())).thenReturn(Future.successful(Option(mockRecipe(2))))

      val result = recipeController.create()(mockRecipeRequest())

      status(result)(MaxAsyncTimeToWait) must equal(OK.intValue)
      contentAsString(result)(MaxAsyncTimeToWait) must
        (include(RecipeCreated) and include("\"id\":2"))
    }
    "return an error message when failed" in {

      when(mockRecipeDataService.save(any())).thenReturn(Future.successful(Option.empty))

      val result = recipeController.create()(mockRecipeRequest())

      status(result)(MaxAsyncTimeToWait) must equal(OK.intValue)
      contentAsString(result)(MaxAsyncTimeToWait) must include(RecipeCreationFailed)
    }
  }

  "A request to update a recipe by id" must {
    "return the updated recipe when the recipe exists" in {

      when(mockRecipeRepository.update(any(), any())).thenReturn(Future.successful(1))

      val result = recipeController.update(1)(mockRecipeRequest())

      status(result)(MaxAsyncTimeToWait) must equal(OK.intValue)
      contentAsString(result)(MaxAsyncTimeToWait) must include(RecipeUpdated)
    }
    "return an error message when failed" in {

      when(mockRecipeRepository.update(any(), any())).thenReturn(Future.successful(0))

      val result = recipeController.update(999)(mockRecipeRequest())

      status(result)(MaxAsyncTimeToWait) must equal(OK.intValue)
      contentAsString(result)(MaxAsyncTimeToWait) must include(SimpleMessageResponse.NotFound)
    }
  }

  "A request to delete a recipe by id" must {
    "return the updated recipe when the recipe exists" in {

      when(mockRecipeRepository.delete(any())).thenReturn(Future.successful(1))

      val result = recipeController.delete(1)(FakeRequest())

      status(result)(MaxAsyncTimeToWait) must equal(OK.intValue)
      contentAsString(result)(MaxAsyncTimeToWait) must include(SimpleMessageResponse.Deleted)
    }
    "return an error message when no recipe with the provided id exists" in {

      when(mockRecipeRepository.update(any(), any())).thenReturn(Future.successful(0))

      val result = recipeController.update(999)(mockRecipeRequest())

      status(result)(MaxAsyncTimeToWait) must equal(OK.intValue)
      contentAsString(result)(MaxAsyncTimeToWait) must include(SimpleMessageResponse.NotFound)
    }
  }
}

package models

import models.RecipeActionResponse.{RecipeCreationFailed, RecipeUpdateFailed}
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import org.scalatestplus.play.PlaySpec

class RecipeActionResponseSpec extends PlaySpec {

  "A RecipeActionResponse initialised with missing fields" must {
    "join the provided strings into the required attribute" in {

      val missingFields = Set("making_time", "serves", "cost")
      val recipeActionResponse = RecipeActionResponse.of(
        RecipeCreationFailed,
        Option(missingFields)
      )

      recipeActionResponse.required should not be None

      missingFields.foreach(field => recipeActionResponse.required.get should include(field))
    }
  }

  "A RecipeActionResponse initialised with no missing fields" must {
    "not set the required attribute" in {

      val recipeActionResponse = RecipeActionResponse.of(RecipeUpdateFailed)

      recipeActionResponse.required shouldBe None
    }
  }
}
